var compiler = require('vue-template-compiler');

exports.fetch = function(load, fetch) {
    // Load the file contents normally, then compile to render function
    return fetch(load)
    .then(function(content){ 
      var compiled = compiler.compileToFunctions(content);

      // Write code to create variable to holder render function
      var code =  "var render = " + compiled.render.toLocaleString() + ";";

      // Write code to create variable to hold list of static render functions
      code += "staticRenderFns = [" + compiled
        .staticRenderFns
        .map(function(f){ return f.toLocaleString(); })
        .join(",");

      code += "];";

      // Export the code
      code +=  "module.exports =  {render: render, staticRenderFns: staticRenderFns}";

      return code;
      
    });
};

exports.translate = function(load) { return load.source; }; 
