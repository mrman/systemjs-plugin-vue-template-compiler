# systemjs-plugin-vue-template-compiler

## Due to break in `jspm-git`, please install this locally, using a git submodule + the `jspm-local` registry ##
The `gitlab` registry isn't working, because `jspm-git` is broken, but it can be sidestepped by adding a git submodule and using the [`jspm-local`](https://www.npmjs.com/package/jspm-local) plugin.

This project simply compiles [Vue](vuejs.org) templates for use in JSPM. While there is a [project for loading `.vue` files](https://github.com/vuejs/systemjs-plugin-vue), this project ONLY seeks to translate text (HTML) to rendered function with SystemJS.

This will allow you to test in the browser (by generating render functions, despite using the runtime library), and when you're ready to bundle

## Installation

To install the template compiler for use in your project (assuming you're using the [Gitlab](https://gitlab.com) repo):

- Install [jspm-git](https://github.com/Orbs/jspm-git) either locally to your project or globally on your machine.

- Install `gitlab` as a repository

``` bash
jspm registry create gitlab jspm-git
# baseurl: ssh://git@gitlab.com
```

- Install systemjs-plugin-vue-template-compiler (this project) from the configured repository

``` bash
jspm install gitlab:mrman/systemjs-plugin-vue-template-compiler
```

~~_NOTE_ This plugin was only tested with `vue@2.1.7`, and `vue-template-compilier@2.1.7` (should match the version number of this project) -- when a PR lands with support for more than one version, this README should be updated.~~
_NOTE_ This plugin has been tested on `vue@2.2.1`, seems to still be working 

After installing the vue template compiler, also make sure to add this to your SystemJS `config.js`:

``` js
SystemJS.config({
  ...

  map: {
    ...
    "vtc": "gitlab:mrman/systemjs-plugin-vue-template-compiler",
    ...
  }

  ...
})
```

That's much shorter than the auto-generated `mrman/systemjs-plugin-vue-template-compiler`, would love to get a PR that finally overwrites the map name.

## Usage

This project enables you to use your compiled templates functions like so:

``` js
... other imports ...
import templateObj from "./template!vtc"

...

app = new Vue({
    ... other properties ...

    render: templateObj.render,
    staticRenderFns: templateObj.staticRenderFns,

    ... other properties
})

...

``` 
